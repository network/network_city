import torch
import torch.nn as nn
from sklearn.metrics import f1_score

from torch.utils.tensorboard import SummaryWriter

from argparse import ArgumentParser
import os
from shutil import rmtree

from utils import *
from models import *

MODEL_CHOICES = ['GCN', 'ChebGNN', 'Baseline', 'LogReg']
HID_CHANNELS=256

def main(model:torch.nn.Module, train_dl, test_dl, name, lr=1e-3, n_epoch=400, device='cuda',
         valid=None):
    model = model.to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr, weight_decay=1e-2)
    p='runs/'+name
    if os.path.isdir(p):
        rmtree(p, )
    writer = SummaryWriter(log_dir=p)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.5)

    for epoch in tqdm(range(n_epoch,), total=n_epoch, desc='Training'):
        train_loss = train_loop(model, train_dl, optimizer, device=device)
        test_loss, f1 = test_loop(model, test_dl, device=device)
        scheduler.step(test_loss)
        # print(model.conv1.lins[0].weight.grad.norm())
        # print(model.conv2.lins[0].weight.grad.norm())
        writer.add_scalar('loss_train',train_loss, epoch)
        writer.add_scalar('loss_test',test_loss, epoch)
        writer.add_scalar('f1/test',f1, epoch)
        if valid is not None:
            val_loss, f1 = test_loop(model, valid, device=device)
            writer.add_scalar('loss_valid',val_loss, epoch)
            writer.add_scalar('f1/valid',f1, epoch)            

def masked_loss(pred, target, ):
    f_mask = target.sum()/target.shape[0] #fraction of edges in route
    loss = F.binary_cross_entropy(pred, target.float(), reduction='none')
    mask = torch.bernoulli(torch.ones_like(loss)*f_mask)
    loss = torch.where(target, loss, loss * mask).sum() *f_mask/2 # mask random non-route edges to balance classes
    # loss = loss.mean()
    return loss

def train_loop(model:Module, train, optimizer:torch.optim.Optimizer, device):
    lavg = 0
    for ibatch, (batch, target) in enumerate(train):
        batch=batch.to(device)
        target=target.to(device)
        optimizer.zero_grad()
        pred = model(batch)
        loss = masked_loss(pred, target)
        loss.backward()
        optimizer.step()
        lavg += loss.detach().item()
    return lavg/(ibatch+1)
    

def test_loop(model, test, device):
    with torch.no_grad():
       lavg = 0
       f1avg = 0
       for ibatch, (batch, target) in enumerate(test):
            batch=batch.to(device)
            target=target.to(device)
            pred = model(batch)
            # print(pred.shape)
            # print(target.shape)
            loss = masked_loss(pred, target)
            lavg += loss
            f1avg+= f1_score(target.cpu().detach(), torch.round( pred.cpu().detach()))
    n=ibatch+1
    return lavg/n, f1avg/n


def get_model(name, eig_n=10):
    dim = eig_n+3
    match name:
        case 'GCN':
            model = EdgeGCN(dim, HID_CHANNELS,)
        case 'ChebGNN':
            model = to_edge_model(ChebGNN)(dim,HID_CHANNELS, k=25)
        case 'Baseline':
            model = Baseline(dim)
        case 'LogReg':
            model=LogReg(dim)
        case _:
            raise ValueError(f'Model not in {MODEL_CHOICES}')
    return model


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('model', choices=MODEL_CHOICES)
    parser.add_argument('run_name', help='Name to store results')
    parser.add_argument('--lr',help='Learning rate', default=5e-4, type=float)
    parser.add_argument('-f' ,help='Frequency of unmasked edges', default=0.5, type=float)
    parser.add_argument('--eig-k',default=20, type=int, 
                        help='Number of eigenvectors to use for features')
    parser.add_argument('--cpu',action='store_true')
    parser.add_argument('--cities',nargs='+', help='Cities to use, if not all of them', default=None)
    parser.add_argument('--valid_cities',nargs='+', help='Cities to use for validation only', default=[])

    args= parser.parse_args()

    model = get_model(args.model, args.eig_k)
    print(model)
    device = 'cuda' if torch.cuda.is_available() and not args.cpu else 'cpu'
    print('Running on ', device)

    networks, routes, stops = get_files(args.cities+args.valid_cities if args.cities is not None else None)
    train_routes, test_routes = train_test_split(routes, )
    print('Processing train data')
    train_dl, train_map, gtrain = make_dataloader(
        networks, train_routes, stops=stops, k_eigen=args.eig_k, device='cpu', hide_prop=args.f,
        ignore_cities=args.valid_cities)
    normalizer = Normalizer(geo_i=2)
    normalizer.fit(gtrain)
    normalizer.transform(gtrain)
    print('Processing test data')
    test_dl, test_map, gtest = make_dataloader(
        networks, test_routes, stops=stops, k_eigen=args.eig_k, device='cpu',  hide_prop=args.f,
        skip_cities=args.valid_cities)
    normalizer.transform(gtest)
    if args.valid_cities:
        val_dl, val_map, gval = make_dataloader(
            networks, test_routes, stops=stops, k_eigen=args.eig_k, device='cpu',  hide_prop=args.f,
            skip_cities=args.cities)
        normalizer.transform(gval)
    else:
        val_dl=None
    main(model, train_dl, test_dl, args.run_name, args.lr, device=device, valid=val_dl)



