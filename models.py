from torch_geometric import nn
from torch_geometric.data import Batch
import torch.nn.functional as F
from torch.nn import Module
import torch
from typing import *

class GCN(Module):
    def __init__(self, in_channels, hid_channels, out_channels=1, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.conv1 = nn.GCNConv(in_channels=in_channels,  out_channels=hid_channels)
        self.conv2 = nn.GCNConv(in_channels=hid_channels, out_channels=out_channels)

    def forward(self,batch:Batch):
        x, ei = batch.x, batch.edge_index
        x = self.conv1(x, ei)
        x = F.relu(x) 
        # x = self.conv2(x, ei)
        # x = F.relu(x)
        x = self.conv2(x, ei) 
        x = F.sigmoid(x)
        return x.squeeze()

class Baseline(Module):
    def __init__(self,dim, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.a = torch.nn.Parameter(torch.rand((dim, dim)))
        
    def forward(self, batch):
        x, ei = batch.x, batch.edge_index
        act = (x[ei[0]] @ self.a * x[ei[1]]).sum(axis=1)
        return torch.sigmoid( act)
    
class LogReg(Module):
    def __init__(self,dim, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.a = torch.nn.Parameter(torch.rand(( dim*2,1)))
        
    def forward(self, batch):
        x, ei = batch.x, batch.edge_index
        act = torch.cat((x[ei[0]], x[ei[1]]), dim=1)
        return torch.sigmoid( act@self.a).squeeze(-1)

class EdgeGCN(GCN):
    def __init__(self, in_channels, hid_channels,  *args, **kwargs) -> None:
        self.dim = 8
        super().__init__(in_channels, hid_channels, self.dim, *args, **kwargs)
    def forward(self, batch: Batch):
        x =  super().forward(batch)
        ei = batch.edge_index
        # print(x.shape)
        # print(ei.shape)
        edges = torch.linalg.vecdot(x[ei[0]], x[ei[1]])
        return edges/self.dim

class ChebGNN(Module):
    def __init__(self, in_channels, hid_channels, out_channels=1, k=5, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.conv1 = nn.ChebConv(in_channels=in_channels,  out_channels=hid_channels, K=k)
        self.conv2 = nn.ChebConv(in_channels=hid_channels, out_channels=hid_channels, K=k)
        self.conv3 = nn.ChebConv(in_channels=hid_channels,  out_channels=hid_channels, K=k)
        self.conv4 = nn.ChebConv(in_channels=hid_channels, out_channels=out_channels, K=k)
    
    def forward(self,batch:Batch):
        x, ei = batch.x, batch.edge_index
        x = self.conv1(x, ei)
        x = F.relu(x) 
        x = self.conv2(x, ei) 
        x = F.relu(x) 
        x = self.conv3(x, ei) 
        x = F.relu(x) 
        x = self.conv4(x, ei) 
        x = F.sigmoid(x)
        return x.squeeze()

def to_edge_model(model_class : Type):
    class EdgeModel(model_class):
        def __init__(self, in_channels, hid_channels,  *args, **kwargs) -> None:
            self.dim = 8
            super().__init__(in_channels, hid_channels, self.dim, *args, **kwargs)
        def forward(self, batch: Batch):
            x =  super().forward(batch)
            ei = batch.edge_index
            edges = torch.linalg.vecdot(x[ei[0]], x[ei[1]])
            return edges/self.dim
    return EdgeModel