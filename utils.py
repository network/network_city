from typing import List, Optional, Sequence, Union
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import networkx as nx
from torch.utils.data.dataloader import _BaseDataLoaderIter
import torch_geometric as tg
from torch_geometric.data import Batch, Data, Dataset
from torch_geometric.data.data import BaseData
from torch_geometric.data.datapipes import DatasetAdapter
from torch_geometric.loader import DataLoader
from torch_geometric.transforms import AddLaplacianEigenvectorPE
from torch_geometric.utils import to_undirected, unbatch
import os
import pandas as pd
import torch
import geopandas as gpd
from typing import *
from tqdm import tqdm
from zipfile import ZipFile
from glob import glob
from collections import defaultdict

# several paradagims are possible
# here, we implement as such :
# each data point is a graph, with one partial route to be completed
# and no other route
# the selection and masking of the route is done by the dataloader
# the task is to predict edges in the route, not nodes 



class Normalizer():
    '''Normalize the data such that :
    - features are standardized across the dataset
    - geographical features, located before geo_i are in comparable ranges across cities'''
    def __init__(self, geo_i = None) -> None:
        self.geo_i = geo_i
        self.m: torch.Tensor = None
        self.s: torch.Tensor = None
        self.geo_m = {}
        self.geo_s = {}

    def fit(self, graphs: List[Data], ):
        '''
        graphs : list of graphs as used by the dataloader'''
        x = []
        x_geo = defaultdict(list)
        for d in graphs:
            x.append(d.x)
            if self.geo_i is not None:
                x_geo[d.city.item()].append(d.x[:,:self.geo_i])
        x = torch.cat(x, 0)
        self.m = x.mean(0)
        self.s = x.std(0)
        if self.geo_i is not None:
            for k, v in x_geo.items():
                v= torch.cat(v,0)
                self.geo_m[k] = v.mean(0)
                self.geo_s[k] = v.std(0)
        
    def transform(self,  graphs: List[Data]):
        '''Normalize data in place'''
        if self.m is None:
            raise RuntimeError('Fit before transforming')
        for d in graphs:
            if self.geo_i is not None:
                d.x[:,self.geo_i:] = (d.x[:,self.geo_i:] - self.m[self.geo_i:])/self.s[self.geo_i:]
                x_city = d.x[:, :self.geo_i]
                d.x[:, :self.geo_i] = (x_city - self.geo_m[d.city.item()])/ self.geo_s[d.city.item()]
            else:
                d.x = (d.x - self.m)/self.std
                

def plot_hist(distrib, ax : plt.Axes = None, title=''):
    if ax is None:
        ax = plt.gca()
    ax.set_title(title)
    y, _, _ = ax.hist(distrib, bins=range(max(distrib)))
    ax.set_xticks(range(max(distrib)))
    return y

def import_graph(city_name, disp=False):
    df = pd.read_csv('data/'+city_name+'/network_combined.csv', sep=';')
    edges = df.iloc[:,0:2]
    if disp:
        print(df.head())
    G = nx.from_pandas_edgelist(edges, source='from_stop_I', target='to_stop_I', create_using=nx.DiGraph)


    dict = {}
    for from_, to_, attr in zip(df.iloc[:,0].to_numpy(), df.iloc[:,1].to_numpy(), df.iloc[:,3].to_numpy()):
        dict[(from_, to_)] = attr
    nx.set_edge_attributes(G, dict, "duration")

    dict = {}
    for from_, to_, attr in zip(df.iloc[:,0].to_numpy(), df.iloc[:,1].to_numpy(), df.iloc[:,2].to_numpy()):
        dict[(from_, to_)] = attr
    nx.set_edge_attributes(G, dict, "dist")

    dict = {}
    for from_, to_, attr in zip(df.iloc[:,0].to_numpy(), df.iloc[:,1].to_numpy(), df.iloc[:,5].to_numpy()):
        dict[(from_, to_)] = attr
    nx.set_edge_attributes(G, dict, "routes")

    return G

def get_routes(network : "pd.Series[str]", edge_index :pd.DataFrame,  routes : Iterable[str] = None) -> pd.DataFrame:
    '''Get the edge-to-route mapping as a [n_edge x n_routes] one-hot encoding
    input : 
        network : 'route_I_counts' column of network dataframe
        routes : series or list of routes to use'''
    network = (
        network.str.split(',') #split lines
        .apply(lambda l : [s.split(':')[0] for s in l] ) #take line names
        .str.join(',') #join
    )
    dummies = network.str.get_dummies(',')
    # dummies = dummies.groupby(edge_index.iloc[:,0]).max() | dummies.groupby(edge_index.iloc[:,1]).max()#edge to node
    # print(network)
    # print('dummies')
    if routes is not None:
        dummies = dummies[[str(r) for r in routes]]
    # print(edge_index['to_stop_I'].max())
    # print(edge_index.shape)
    # print(dummies)
    return dummies


class RouteMaskingDataloader(DataLoader):
    """
    hide_prop : proportion of unmasked nodes in the route"""
    def __init__(self, dataset, batch_size: int = 1, shuffle: bool = False, hide_prop=0.25, follow_batch: List[str] | None = None, exclude_keys: List[str] | None = None, **kwargs):
        super().__init__(dataset, batch_size, shuffle, follow_batch, exclude_keys, **kwargs)
        self.hide_prop = hide_prop

    def __iter__(self) -> _BaseDataLoaderIter:
        for batch in  super().__iter__():
            true_route = batch.routes
            filt = torch.bernoulli(torch.ones_like(true_route)*self.hide_prop)
            masked_route = torch.minimum(true_route, filt) # 1 iff both are 1
            batch.routes = masked_route
            route_x = torch.zeros((batch.x.shape[0], 1))
            route_x[batch.edge_index[0,masked_route==1]] = 1
            route_x[batch.edge_index[1,masked_route==1]] = 1
            batch.x = torch.cat([batch.x, route_x], 1)
            yield batch, true_route

#todo: change such that only one call to this function is necessary
def make_dataloader(
        networks : Dict[str,pd.DataFrame], routes:Dict[str, list] = None, 
        stops: Dict[str,gpd.GeoDataFrame]=None,
        batch_size=5, k_eigen=10,
        hide_prop=0.25, device='cpu',
        ignore_cities=[], 
        skip_cities = [],
        use_geometry=True
        ) -> DataLoader:
    '''
    networks : dataframes as extracted by get_files()
    routes : routes to keep in the dataset. If None, all routes are kept
    hide_prop : proportions of non-hidden stops
    ignore_cities : do not include these cities in the dataloader, but keep them in graphs for normalization
    skip_cities : completely skip these cities
    '''
    graphs = []
    graphs_out = []
    city_mapping = {}
    transform = AddLaplacianEigenvectorPE(k_eigen, None, is_undirected=True)
    n_cities = len([*networks.keys()]) 
    wrapper = tqdm(enumerate(networks.items()), total=n_cities,)
    count=0
    for i, (city, network) in wrapper:
        if city in skip_cities :
            continue
        wrapper.set_postfix_str(city)
        routes_dummies = torch.tensor(get_routes(
            network['route_I_counts'],
            network[['from_stop_I','to_stop_I']],
            routes[city]['route_I'].unique() if routes is not None else None
            ).to_numpy()).to(device)
        # get geographical features
        if use_geometry:
            xy = stops[city].geometry
            x = torch.stack([torch.tensor(xy.x), torch.tensor(xy.y)], dim=1).float()
        else:
            x = None
        #reindex edge index
        map_i=stops[city].set_index('stop_I').id
        ei = network[['from_stop_I','to_stop_I']].to_numpy().T
        ei = np.stack ((map_i.loc[ei[0]].to_numpy(), map_i.loc[ei[1]].to_numpy()))
        ei = torch.tensor(ei).to(device)
        # dummy graph to compute transforms only once
        ei, routes_dummies = to_undirected(ei, routes_dummies, reduce='max', num_nodes=ei.max()+1)
        g = Data(x, ei, num_nodes=ei.max()+1).to(device) #todo : why does this not change x ?
        g = transform(g)
        x = g.x.to(device)
        for route_col in routes_dummies.T :
            count+=1
            graph = Data(
                x = torch.clone(x), # to avoid repeat modification of the same variable when normalizing 
                edge_index=ei,
                city = torch.tensor(i).to(device),
                routes = route_col.bool()
                ) 
            graphs_out.append(graph)
            if city not in ignore_cities: #ignore cities for validation
                graphs.append(graph)
        city_mapping[i] = city
    if  not hide_prop :
        dl = DataLoader(graphs, batch_size=batch_size, shuffle=True, )
    else :
        dl = RouteMaskingDataloader(graphs, batch_size, shuffle=True, hide_prop=hide_prop)
    print(f'Prepared dataloader with {count} routes across {n_cities} cities')
    return dl,city_mapping, graphs_out

def get_files(cities = None) -> Dict[str, Dict[str, pd.DataFrame]]:
    dirs = glob('data/*.zip')
    networks = {}
    routes = {}
    stops={}
    for p in dirs:
        archive = ZipFile(p, mode='r')
        city = os.path.basename(p).split('.')[0]
        if cities is not None and not city in cities:
            continue
        with archive.open(city+'/network_combined.csv') as file:
            networks[city] = pd.read_csv( file, sep=';')
        with archive.open(city+'/routes.geojson') as file:
            routes[city] = gpd.read_file(file)
        with archive.open(city+'/stops.geojson') as file:
            df =  gpd.read_file(file)
            df[['stop_I','id']] = df[['stop_I','id']].astype(int)
            stops[city] = df
    return networks, routes, stops

def train_test_split(routes:Dict[str, pd.DataFrame], p=0.8):
    '''
    Split routes between train and test
    '''
    # train_df = {}
    # test_df = {}
    train = {}
    test = {}
    for city, c_routes in routes.items():
        i = int(c_routes.shape[0] * p) 
        c_routes = c_routes.sample(frac=1, ) #shuffle
        train[city]  = c_routes.iloc[:i]
        test [city]  = c_routes.iloc[i:]
    return train, test, 

        

        
