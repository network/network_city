from numpy.random import choice
from glob import glob
import os

cities = [os.path.basename(p).split('.')[0] for p in glob('data/*.zip')]

for i in range(5):
    print(cities[:6])
    command = f'python main.py ChebGNN Cheb_{cities[6+i]} -f 0.8 --cities {" ".join(cities[:6])} --valid_cities {cities[6+i]}'
    print(command)
    os.system(command)