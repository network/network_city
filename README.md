# Network_city



## Prerequisites

The required python version as well as the list of the necessary libraries are described in ``requirement.txt``

## How to use 
### Graph exploration
The graph exploration is implemented in the ``Read_data.ipynb`` notebook. 

The notebook implement a way to save and load graph models as well as  their features. The files are saved in the ``save`` folder.
### Graph exploitation

How to run the code :
```
python  main.py model_type run_name  [--lr LR] [-f F] [--eig-k EIG_K] [--cpu] [--cities CITIES [CITIES ...]] [--valid_cities VALID_CITIES [VALID_CITIES ...]]
               

positional arguments:
  model_type in {GCN,ChebGNN,Baseline,LogReg} 
  run_name              Name to store results

options:
  --lr LR               Learning rate. Default : 5e-4
  -f F                  Frequency of unmasked edges. Default : 0.5
  --eig-k EIG_K         Number of eigenvectors to use for features. Default:10
  --cpu                 If flag is passed, use cpu instead of trying to use gpu
  --cities CITIES [CITIES ...]
                        Cities to use for training and testing. Default : all of them
  --valid_cities VALID_CITIES [VALID_CITIES ...]
                        Cities to use for testing only. Default : None
```

Training can be visualized using tensorboard.

